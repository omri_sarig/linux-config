#!/bin/bash
cp -uv ~/.vimrc .
cp -uv ~/.bashrc .
cp -uv ~/.tmux.conf .
cp -uv ~/.gitconfig .
cp -uvpR ~/.vim .
cp -uvpR ~/.config/tmux config/
cp -uvpR ~/.config/git config/
rm -r ./.vim/swap_dir
rm -r ./.vim/undodir
rm -r ./.vim/view
