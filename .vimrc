
" Vundle condig

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'honza/vim-snippets'
Plugin 'scrooloose/syntastic'
Plugin 'valloric/youcompleteme'
Plugin 'tpope/vim-fugitive'
Plugin 'kien/ctrlp.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'easymotion/vim-easymotion'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'brookhong/cscope.vim'
Plugin 'hari-rangarajan/cctree'
"Plugin 'davits/dyevim'
"Plugin 'jeaye/color_coded'
Plugin 'SirVer/ultisnips'
"Plugin 'apalmer1377/factorus'
"Plugin 'luchermitte/vim-refactor'
"Plugin 'renamec.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'airblade/vim-gitgutter'
Plugin 'flazz/vim-colorschemes'
Plugin 'haya14busa/incsearch.vim'
Plugin 'haya14busa/incsearch-fuzzy.vim'
Plugin 'dominikduda/vim_current_word'
Plugin 'google/vim-searchindex'
Plugin 'sjl/gundo.vim'
Plugin 'octref/rootignore'
Plugin 'klen/python-mode'
Plugin 'simeji/winresizer'
Plugin 'FelikZ/ctrlp-py-matcher'
Plugin 'mhinz/vim-grepper'
Plugin 'jacquesbh/vim-showmarks'
Plugin 'pseewald/vim-anyfold'
Plugin 'w0rp/ale'
Plugin 'godlygeek/tabular'
Plugin 'rdnetto/YCM-Generator'
Plugin 'liuchengxu/vim-which-key'
Plugin 'arthurxavierx/vim-caser'
Plugin 'tpope/vim-surround'
Plugin 'vim-scripts/marvim'
Plugin 'aklt/plantuml-syntax'
Plugin 'scrooloose/vim-slumlord'
Plugin 'tpope/vim-obsession'
Plugin 'dhruvasagar/vim-prosession'
Plugin 'gikmx/vim-ctrlposession'
Plugin 'kracejic/themeinabox.vim'
Plugin 'bronson/vim-visual-star-search'
Plugin 'unblevable/quick-scope'
"Plugin 'kana/vim-textobj-lastpat'
"Plugin 'vim-scripts/textobj-fatpack'

" Plugin 'vcscommand.vim'

" Plugin 'exUtility'
" Plugin 'CRefVim'

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Syntastic config
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_c_checkers = ['gcc', 'clang_check', 'clang_tidy', 'cppclean', 'make']

set splitright
set nosplitbelow

" Turn on syntax highlighting
syntax on

" For plugins to load correctly
filetype plugin indent on

let mapleader = "\\"

" Security
set modelines=0

" Show file stats
set ruler

" Stop vim from giving annoying messages (with beeping or blinking)
set vb t_vb=

" Encoding
set encoding=utf-8

" Whitespace
set textwidth=80
autocmd bufreadpre *.py setlocal textwidth=80
autocmd bufreadpre *.txt setlocal textwidth=0
autocmd bufreadpre *.csv setlocal textwidth=0
set formatoptions=tcqrn1
set tabstop=4
set expandtab
set shiftwidth=2
set softtabstop=4
set noshiftround
set colorcolumn=-0

" Cursor motion
set scrolloff=3
set backspace=indent,eol,start
set matchpairs+=<:> " use % to jump between pairs
runtime! macros/matchit.vim

" Allow hidden buffers
set hidden

" Rendering
set ttyfast

" Status bar
set laststatus=2

" Last line
set showmode
set showcmd

" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch
"map <leader>\ :let @/=''<cr> " clear search

" Remap help key.
inoremap <F1> <ESC>:set invfullscreen<CR>a
nnoremap <F1> :set invfullscreen<CR>
vnoremap <F1> :set invfullscreen<CR>

" Textmate holdouts

" Visualize tabs and newlines
set listchars=tab:▸\ ,eol:¬

" Color scheme (terminal)
set t_Co=256
set background=dark
let g:solarized_termcolors=256
let g:solarized_termtrans=1

" Set changing the size of the windows
noremap <C-w>L 5<C-w>>
noremap <C-w>H 5<C-w><

" Move easily to any tab you want
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 1gt<C-p>

" Map home end end to Ctrl and move
noremap <C-l> <End>
noremap <C-h> <Home>

" English spell checks
setlocal spell spelllang=en_us

" don't wrap lines
set nowrap

" filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

let g:ctrlp_map = '<c-e>'
noremap <leader>e :CtrlPCurFile<CR>

" Default mapping
noremap <C-d> <Nop>
let g:multi_cursor_use_default_mapping = 0
let g:multi_cursor_start_key           = '<C-d>'
let g:multi_cursor_start_word_key      = '<C-d>'
let g:multi_cursor_next_key            = '<C-n>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '`'

" Cscope vim config
nnoremap <leader>bc :!cscope -b *.c *.h */*.c */*.h */*/*.c */*/*.h /usr/include/*.h /usr/include/*/*.h /usr/include/*/*/*.h /usr/include/*/*/*/*.h /usr/include/*/*/*/*/*.h /usr/include/*/*/*/*/*.h<CR>
nnoremap <leader>hn :call CscopeFindInteractive(expand('<cword>'))<CR>
nnoremap <leader>l :call ToggleLocationList()<CR>
" s: Find this C symbol
nnoremap  <leader>ns :vsplit<CR>:call CscopeFind('s', expand('<cword>'))<CR>
" g: Find this definition
nnoremap  <leader>ng :vsplit<CR>:call CscopeFind('g', expand('<cword>'))<CR>
" d: Find functions called by this function
nnoremap  <leader>nd :vsplit<CR>:call CscopeFind('d', expand('<cword>'))<CR>
" c: Find functions calling this function
nnoremap  <leader>nc :vsplit<CR>:call CscopeFind('c', expand('<cword>'))<CR>
" t: Find this text string
nnoremap  <leader>nt :vsplit<CR>:call CscopeFind('t', expand('<cword>'))<CR>
" e: Find this egrep pattern
nnoremap  <leader>ne :vsplit<CR>:call CscopeFind('e', expand('<cword>'))<CR>
" f: Find this file
nnoremap  <leader>nf :vsplit<CR>:call CscopeFind('f', expand('<cword>'))<CR>
" i: Find files #including this file
nnoremap  <leader>ni :vsplit<CR>:call CscopeFind('i', expand('<cword>'))<CR>
let g:cscope_silent = 1

" Git mapping
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gl :Git log<CR>
nnoremap <leader>gc :Gcommit<CR>
nnoremap <leader>gw :Gwrite<CR>

nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi

" Remove trailing spaces.
noremap <leader>rts :%s/\s\+$//e<CR>

" Expand types in file
noremap <leader>tot :%s/    /\t/g<CR>

" Highlight trailing spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" replace word
noremap <leader>as :%s/<C-r>"/

" General plugins config
noremap <F2> :NERDTreeToggle<CR>
noremap <leader>gu :GundoToggle<CR>
let g:pymode = 1
let g:pymode_run = 1
let g:pymode_run_bind = '<leader>r'

" <Leader>f{char} to move to {char}
noremap  <Leader>f <Plug>(easymotion-bd-f)
nnoremap <Leader>f <Plug>(easymotion-overwin-f)
noremap <Leader>L <Plug>(easymotion-bd-jk)
nnoremap <Leader>L <Plug>(easymotion-overwin-line)
noremap  <Leader>w <Plug>(easymotion-bd-w)
nnoremap <Leader>w <Plug>(easymotion-overwin-w)

let g:UltiSnipsExpandTrigger="<c-j>"

noremap <Leader>grep :grep -R "" .<CR>
noremap <Leader>pgrep :grep -R """ .<CR>
noremap <leader>/ :nohlsearch<CR>

noremap <leader>j o<Esc>k
noremap <leader>k O<Esc>j

vnoremap < <gv
vnoremap > >gv

set undofile " Maintain undo history between sessions
set undodir=~/.vim/undodir
set dir=~/.vim/swapdir
let g:airline_theme='molokai'
let g:ycm_server_python_interpreter='/usr/bin/python3'

" Skeletons
function! CheckHFile()
let filename = expand("%:t:r")
if (getline("1") !~ '#ifndef _'.filename)
  exe "normal!ggO#IFNDEF __".filename."__"
  exe "normal!o#DEFINE __".filename."__"
  exe "normal!3o"
  exe "normal!o#ENDIF /* __".filename."__ */"
  exe "normal!ggg~GGdd"
  exe "normal!ggOfh"
  exe ":1"
endif
endfunction

function! CheckCppFile()
let filename = expand("%:t")
exe "normal!ggO// ".filename." -- "
endfunction

function! CheckPyFile()
if (getline("1") !~ "if '__main__' == __name__:")
  normal!ggOif '__main__' == __name__:
  normal!o    main()
endif
endfunction

autocmd BufNewFile *.h  :call CheckHFile()
autocmd BufNewFile *.hpp  :call CheckHFile()
autocmd BufNewFile *.py :call CheckPyFile()
autocmd BufNewFile *.cpp :call CheckCppFile()

" General config
inoremap <C-b> <Esc>:set paste<cr>o<esc>:set nopaste<cr>o <Esc>kdd$s
noremap <C-w>f :vertical wincmd f<CR>

" basic compile and run
command Make make "%:t:r"
command Run !"./%:t:r"
command Clean !rm "%:t:r"

" More vim configs
source ~/.vim/personal/a.vim

" you complete me (ycm) more config
noremap <leader>fix :YcmCompleter FixIt<CR>
noremap <leader>nf :YcmDiags<CR> <CR><leader>fix
noremap <leader>af :g/^.*$/YcmCompleter FixIt<CR><leader>/
"let g:ycm_extra_conf_globlist = ['~/Projects/*']
autocmd BufWritePost * GitGutter

function! s:config_fuzzyall(...) abort
  return extend(copy({
  \   'converters': [
  \     incsearch#config#fuzzy#converter(),
  \     incsearch#config#fuzzyspell#converter()
  \   ],
  \ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> z/ incsearch#go(<SID>config_fuzzyall())
noremap <silent><expr> z? incsearch#go(<SID>config_fuzzyall({'command': '?'}))
noremap <silent><expr> zg? incsearch#go(<SID>config_fuzzyall({'is_stay': 1}))

hi CurrentWord ctermbg=53
hi CurrentWordTwins ctermbg=245

" If you want to start window resize mode by `Ctrl+T`
let g:winresizer_start_key = '<C-q>'
noremap <leader>rw :WinResizerStartResize<CR>

function RandomColorScheme()
  let mycolors = split(globpath("~/.vim/colors","*"),"\n") 
  exe 'so ' . mycolors[localtime() % len(mycolors)]
  unlet mycolors
endfunction

call RandomColorScheme()

:command NewColor call RandomColorScheme()

autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview

nnoremap <leader>as  :Grepper -tool ag -side -query ""
nnoremap <leader>ar  :Grepper -tool ag -query ""
nnoremap <leader>my :DoShowMarks!<CR>
nnoremap <leader>mn :NoShowMarks!<CR>

autocmd Filetype * AnyFoldActivate
set foldlevel=0

onoremap <silent> aa :<C-U>normal! ggvG<CR>
vnoremap <silent> aa :<C-U>normal! ggvG<CR>

nnoremap <silent> <leader> :WhichKey '<leader>'<CR>
set timeoutlen=100

packadd termdebug
set lazyredraw

" Set tabs
noremap <C-t> :tabnew<CR>
noremap <C-n> :tabn<CR>
noremap <C-p> :tabp<CR>

cnoremap <c-p> <Up>
cnoremap <c-n> <Down>

noremap <leader>rts :%s/\s\+$//e<CR>

runtime! ftplugin/man.vim
noremap K <leader>K

set wildmode=longest,list,full
set wildmenu

command W :windo w
command Q :windo q
command WQ :windo wq
command Wq :wq

" Marvim support
let marvim_store = '/home/omri/.vim/marvim'
let marvim_find_key = '<F4>'
let marvim_store_key = '<F5>'

if (&ft == 'uml')
    set makeprg="java -jar /home/omri/.vim/bundle/vim-slumlord/plantuml.jar -tsvg %"
endif

" general
vnoremap <c-u> U
inoremap hh <Esc>
set timeoutlen=500
